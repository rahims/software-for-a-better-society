import React from "react";
import { Navigation } from "../../components/globals/Navigation";
import {
  H1,
  Row,
  Wrapper,
  CustomContainer,
  CustomWidthWrapper
} from "../../components/globals";

export default function TDT10() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation>
            <a href="/">home</a>&#47;<a href="/teaching">teaching</a>&#47;
            <a href="/tdt10">tdt10</a>
          </Navigation>
          <CustomContainer>
            <H1>TDT10-Gender and diversity in software development</H1>
            The page has been moved to{" "}
            <a
              href="https://i.ntnu.no/wiki/-/wiki/Norsk/TDT10+-+Gender+and+diversity+in+software+development"
              target="_blank"
              rel="noopener noreferrer"
            >
              the department official page
            </a>
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  );
}
