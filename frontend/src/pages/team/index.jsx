import React from "react";
import { TeamMembers } from "./TeamElements";
import { Navigation } from "../../components/globals/Navigation";
import { Wrapper, Row, H1, CustomWidthWrapper } from "../../components/globals";

export default function Team() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation>
            <a href="/">home</a>&#47;<a href="/team">team</a>
          </Navigation>
          <H1>Team</H1>
          {TeamMembers}
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  );
}
