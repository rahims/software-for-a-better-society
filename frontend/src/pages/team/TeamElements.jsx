import React from "react";
import { Members, PersonContainer, Person, SocialMedia } from "./elements";

import Mail from "./static/Mail.svg";
import Gate from "./static/Gate.svg";
import Google from "./static/Google.svg";
import Twitter from "./static/Twitter.svg";
// import Scopus from "./static/Scopus.svg";
import Linkedin from "./static/Linkedin.svg";
import Facebook from "./static/Facebook.svg";

export const TeamMembers = (
  <>
    <Members>
      <h4>Principal investigator</h4>
      <PersonContainer>
        <Person>
          <h5>Letizia Jaccheri</h5>
          <SocialMedia>
            <small>
              <a href="mailto:letizia.jaccheri@ntnu.no">
                <img src={Mail} alt="Mail" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://scholar.google.no/citations?user=xBnjjysAAAAJ&hl=en&oi=ao"
              >
                <img src={Google} alt="GoogleScholar" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/letizia/?originalSubdomain=no"
              >
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.facebook.com/letizia.jaccheri"
              >
                <img src={Facebook} alt="Facebook" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://twitter.com/letiziajaccheri"
              >
                <img src={Twitter} alt="Twitter" />
              </a>
            </small>
          </SocialMedia>
        </Person>
      </PersonContainer>
    </Members>

    <Members>
      <h4>Post-doctoral students</h4>
      <PersonContainer>
        <Person>
          <h5>Anna Szlavi</h5>
          <small>
            gender balance, inclusion,
            intersectionality in computer science
          </small>
          <SocialMedia>
            <small>
              <a href="mailto:anna.szlavi@ntnu.no">
                <img src={Mail} alt="Mail" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://scholar.google.com/citations?user=X7W8snQAAAAJ&hl=en"
              >
                <img src={Google} alt="GoogleScholar" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/annaszlavi"
              >
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.researchgate.net/profile/Anna-Szlavi"
              >
                <img src={Gate} alt="Gate" />
              </a>
            </small>
          </SocialMedia>
        </Person>
      </PersonContainer>
    </Members>

    <Members>
      <h4>PhD students</h4>
      <PersonContainer>
        <Person>
          <h5>Marte Hoff Hagen</h5>
          <small>
            Designing Digital Rehabilitation Technology for Survivors of
            Childhood Critical Illness
          </small>
          <SocialMedia>
            <small>
              <a href="mailto:marte.h.hagen@ntnu.no">
                <img src={Mail} alt="Mail" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://scholar.google.com/citations?user=SVcb24wAAAAJ&hl=no"
              >
                <img src={Google} alt="GoogleScholar" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/martehoffhagen/"
              >
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Ibrahim El Shemy</h5>
          <small>
            Augmented Reality for Language Learning in Children with Autism
            Spectrum Disorder
          </small>
          <SocialMedia>
            <small>
              <a href="mailto:ibrahim.elshemy@mail.polimi.it">
                <img src={Mail} alt="Mail" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://it.linkedin.com/in/ibrahim-el-shemy-5a3582172"
              >
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.researchgate.net/profile/Ibrahim-El-Shemy"
              >
                <img src={Gate} alt="Gate" />
              </a>
            </small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Claudia Maria Cutrupi</h5>
          <small>
            Designing interventions for Diversity in Computer Science
          </small>
          <SocialMedia>
            <small>
              <a href="mailto:claudia.m.cutrupi@ntnu.no">
                <img src={Mail} alt="Mail" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://scholar.google.com/citations?user=baavJ0AAAAAJ&hl=it"
              >
                <img src={Google} alt="GoogleScholar" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/claudiamariacutrupi/"
              >
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Ana Carolina Moises de Souza</h5>
          <small>Sustainability</small>
          <SocialMedia>
            <small>
              <a href="mailto:ana.c.m.de.souza@ntnu.no">
                <img src={Mail} alt="Mail" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/ana-carolina-moises-de-souza-65b15616/"
              >
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </small>
          </SocialMedia>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Tanya Mangion</h5>
          <small>Digital tools and Climate Changes</small>
          <SocialMedia>
            <small>
              <a href="mailto:tanya.mangion@ntnu.no">
                <img src={Mail} alt="Mail" />
              </a>
            </small>
            <small>
              <a
                target="_blank"
                rel="noopener noreferrer"
                href="https://www.linkedin.com/in/tanya-mangion-ba469a221/"
              >
                <img src={Linkedin} alt="Linkedin" />
              </a>
            </small>
          </SocialMedia>
        </Person>
      </PersonContainer>
    </Members>

    <Members>
      <h4>Co-supervisors and mentors for the group</h4>
      <PersonContainer>
        <Person>
          <h5>Professor Michail Giannakos</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Associate Professor Sofia Papavlasopoulou</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Daniela Cruzes</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Alexander Serebrenik</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Gunnar Hartvigsen</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Ilias Pappas</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Associate Professor Simone Mora</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Annemie Wyckmans</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Mila Dimitrova Vulchanova</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Patricia Lago</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Professor Anh Nguyen Duc</h5>
        </Person>
      </PersonContainer>
    </Members>

    <Members>
      <h4>Past members</h4>
      <PersonContainer>
        <Person>
          <h5>Alicia Julia Wilson Takaoka</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Jose David Paton </h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Mara-Gabriela Diaconu</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Farzana Quayyum</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Leif Erik Opland</h5>
        </Person>
      </PersonContainer>

      <PersonContainer>
        <Person>
          <h5>Orges Cico</h5>
        </Person>
      </PersonContainer>

       <PersonContainer>
        <Person>
          <h5>Juan Carlos Torrado Vidal</h5>
        </Person>
      </PersonContainer>

       <PersonContainer>
        <Person>
          <h5>Javier Gomez</h5>
        </Person>
      </PersonContainer>
    </Members>
  </>
);
