import React         from 'react';
import { Navigation} from '../../components/globals/Navigation';
import { Wrapper, Row, H1, CustomWidthWrapper, ImageWrapper, CustomContainer }           from '../../components/globals';
import { MasterSetup } from './elements';
import students from './static/students.webp';

export default function Master() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/master">master</a></Navigation>
          <CustomContainer>
            <H1>Master Students</H1>
            <ImageWrapper><img src={students} alt="group"/></ImageWrapper>
            {MasterSetup}
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
