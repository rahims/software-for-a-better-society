import React from 'react';
import { Navigation } from '../../components/globals/Navigation';
import { Wrapper, Row, H1, CustomWidthWrapper, CustomContainer } from '../../components/globals';
import { VideoWrapper } from '../videos/elements';

export default function TDT4290() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/teaching">teaching</a>&#47;<a href="/tdt4290">tdt4290</a></Navigation>
          <CustomContainer>
            <H1>TDT4290–Customer Driven Project</H1>
            <p>Each group is given a task from a client that is to be carried out as a project. All phases of a development project are to be covered: Preliminary studies, requirements specification, design, implementation, and evaluation. The emphasis is on the early phases. It is important that the groups work in close collaboration with the client. The groups will hand in a project report and give a final presentation and demonstration of a runnable system to the client and the evaluator.</p>
            <p>See the presentation to Customer <a target="_blank" rel="noopener noreferrer" href="https://letiziajaccherihp.files.wordpress.com/2021/05/customer-2021-letizia-anh-13-04-2021.pdf">here.</a></p>
            <p>For proposing a project see <a target="_blank" rel="noopener noreferrer" href="https://forms.office.com/Pages/ResponsePage.aspx?id=cgahCS-CZ0SluluzdZZ8BdwH3NbCxqFGjQV3RVA4ypBUMTJZUTcwRTFWRThDSjNPRVVOSjJSV0pHQy4u">here.</a></p>
            <p>Official TDT4290 Course page - NTNU: see <a href="https://www.ntnu.edu/studies/courses/TDT4290#tab=omEmnet" target="_blank" rel="noopener noreferrer">here.</a></p>
            <p>Compendium <a href="https://drive.google.com/file/d/1OVgco3q47z5aGDicylaFau3Vu_3cqsUN/view?usp=sharing" target="_blank" rel="noopener noreferrer">download.</a></p>
            <VideoWrapper>
              <iframe width="560" height="315" src="https://www.youtube.com/embed/xpdIbqjxHE0" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen />
            </VideoWrapper>
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
