import React                   from 'react';
import { Navigation }          from '../../components/globals/Navigation';
import { ActiveProjectsTable } from './elements';
import { Wrapper, Row, H1, CustomWidthWrapper, CustomContainer } from '../../components/globals';


export default function CompletedProjects() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/projects">projects</a>&#47;<a href="/completed-projects">completed</a></Navigation>
          <CustomContainer>
            <H1>Completed Projects</H1>
            <ActiveProjectsTable cellPadding={10} cellSpacing={10}>
              <tbody>


                <tr>
                  <td>BALANSE&nbsp;<a href="https://www.ntnu.edu/idun" target="_blank" rel="noopener noreferrer">IDUN</a></td>
                  <td>From PhD to Professor</td>
                  <td>NFR 2019 - 2022</td>
                  <td><a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.edu/employees/letizia.jaccheri">Letizia Jaccheri</a>&nbsp;- <a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.no/ansatte/mara.diaconu">Mara Gabriela Diaconu</a></td>
                </tr>


                <tr>
                  <td><a href="https://cordis.europa.eu/project/rcn/209818/en" target="_blank" rel="noopener noreferrer">INITIATE</a></td>
                  <td>INnovation through bIg daTa and socIal enTreprEneurship</td>
                  <td>H2020 2018 – 2021</td>
                  <td><a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.edu/employees/letizia">Letizia Jaccheri</a>&nbsp;- <a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.edu/employees/ilpappas">Ilias Pappas</a></td>
                </tr>
            
                <tr>
                  <td><a href="https://www.forskningsradet.no/prognett-internasjonale-stipend/Nyheter/Resultater_fra_INTPART_utlysningen_2017/1254031707611/p1224697968393" target="_blank" rel="noopener noreferrer">INTPART</a>&nbsp;IPIT</td>
                  <td>International Partnership in Information Technology</td>
                  <td>NFR 2017 – 2021</td>
                  <td><a href="https://ipit.network/" target="_blank" rel="noopener noreferrer">Letizia Jaccheri</a>&nbsp;- <a target="_blank" rel="noopener noreferrer" href="https://www.ntnu.edu/employees/jingyue.li">Jingyue Li</a></td>
                </tr>


              </tbody>
            </ActiveProjectsTable>
          </CustomContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}
