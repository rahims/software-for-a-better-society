import { CooperatorCard, CooperatorsContainer } from "./elements"
import { CustomWidthWrapper, CustomContainer, Wrapper, H1, Row } from "../../components/globals"
import { Navigation } from "../../components/globals/Navigation"
import reddbarna from './static/reddbarna.svg'
import leaplearning from './static/leaplearning.jpg'
import yme from './static/yme.png'
import smidig from './static/smidig.svg'
import eladda from './static/ELADDA.jpeg'
import s2 from './static/S2.png'
import HIT from './static/HIT.png'

export default function Cooperators() {
  return (
    <Wrapper>
      <Row>
        <CustomWidthWrapper>
          <Navigation><a href="/">home</a>&#47;<a href="/cooperators">cooperators</a></Navigation>
          <CustomContainer>
            <H1>Cooperators</H1>
          </CustomContainer>
          <CooperatorsContainer>
            <a href="https://www.reddbarna.no/" target="_blank" rel="noopener noreferrer">
              <CooperatorCard><img src={reddbarna} alt="reddbarna" loading="eager" /></CooperatorCard>
            </a>
            <a href="https://leaplearning.no/" target="_blank" rel="noopener noreferrer">
              <CooperatorCard><img src={leaplearning} alt="leaplearning" loading="eager" /></CooperatorCard>
            </a>
            <a href="https://www.yme.no/" target="_blank" rel="noopener noreferrer">
              <CooperatorCard><img src={yme} alt="yme" loading="eager" /></CooperatorCard>
            </a>
            <a href="https://smidig.org/" target="_blank" rel="noopener noreferrer">
              <CooperatorCard><img src={smidig} alt="smidig" loading="eager" /></CooperatorCard>
            </a>
            <a href="https://www.ntnu.edu/e-ladda" target="_blank" rel="noopener noreferrer">
              <CooperatorCard><img src={eladda} alt="e-ladda" loading="eager" /></CooperatorCard>
            </a>
            <a href="https://s2group.cs.vu.nl/" target="_blank" rel="noopener noreferrer">
              <CooperatorCard><img src={s2} alt="s2" loading="eager" /></CooperatorCard>
            </a>
            <a href="https://site.uit.no/hit/" target="_blank" rel="noopener noreferrer">
              <CooperatorCard><img src={HIT} alt="HIT" loading="eager" /></CooperatorCard>
            </a>
          </CooperatorsContainer>
        </CustomWidthWrapper>
      </Row>
    </Wrapper>
  )
}